import 'package:flutter/material.dart';

class app4 extends StatelessWidget {
  const app4({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          width: 150,
          height: 150,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.black,
              boxShadow: const [
                BoxShadow(
                    offset: Offset(10, 10), color: Colors.red, blurRadius: 10)
              ]),
        ),
      ),
    );
  }
}
