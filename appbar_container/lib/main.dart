import 'package:flutter/material.dart';
import 'package:appbar_container/app0.dart';
import 'package:appbar_container/app1.dart';
import 'package:appbar_container/app2.dart';
import 'package:appbar_container/app3.dart';
import 'package:appbar_container/app4.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: app4(),
    );
  }
}
