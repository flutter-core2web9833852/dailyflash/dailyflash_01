import 'package:flutter/material.dart';

class app1 extends StatelessWidget {
  const app1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const Icon(Icons.access_alarm),
        actions: const [
          Icon(
            Icons.add_box,
          ),
          Icon(Icons.accessibility_new_rounded),
          Icon(Icons.account_circle_outlined)
        ],
        title: const Text("Appbar"),
        centerTitle: true,
        backgroundColor: Colors.limeAccent,
      ),
    );
  }
}
