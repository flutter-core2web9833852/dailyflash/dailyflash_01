import 'package:flutter/material.dart';

class app0 extends StatelessWidget {
  const app0({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const Icon(Icons.access_alarm),
        actions: const [
          Icon(Icons.add_box),
        ],
        title: const Text("Appbar"),
        centerTitle: true,
      ),
    );
  }
}
